FROM node:17-alpine3.13
WORKDIR /firebase-angular-builder
RUN npm install -g firebase-tools
RUN npm install -g @angular/cli
RUN npm install typescript -g
RUN npm install firebase-functions
RUN npm install firebase-admin


